﻿using System;
using System.Threading.Tasks;
using NordeaProject.Data;
using NordeaProject.DataSource;
using NordeaProject.WebServicesConnectors;

namespace NordeaProject
{ 

    class Program
    {
        //public static async Task Main(string[] args)
        static void Main(string[] args)
        {
            var task1 = new TaskOne(new PriceDataSource(), new PositionDataSource());
            var result1 = task1.CalculateMarketValue();
            foreach (var position in result1)
            {
                Console.WriteLine($"Value for position id {position.Key.PositionId} value: {position.Value}");
            }

            var task2 = new TaskTwo(new WebServiceOneConnector(), new WebServiceTwoConnector(), new WebServiceThreeConnector());
            var result2 = task2.GetPrices().GetAwaiter().GetResult();

            PriceManager manager = new PriceManager(new PriceDataSource());
            var result3 = manager.GetPrices();

            Console.ReadLine();

        }
    }
}
