﻿using System;
using System.Linq;
using NordeaProject.Data;

namespace NordeaProject.DataSource
{

    public class PositionDataSource: IPositionDataSource
    {
        public Position[] GetPositions()
        {
            var date = new DateTime(2000, 01, 01);
            Random randNum = new Random();
            //TODO Could be duplicated -> but generating this data are not part of task i hope:)
            return Enumerable
                .Repeat(0, randNum.Next(1000, 10000))
                .Select(i => new Position(randNum.Next(0, 100), date.AddDays(randNum.Next(0, 1000)), i, randNum.Next(0, 1000).ToString()))
                .ToArray();
        }
        
    }

}
