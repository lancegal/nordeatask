﻿using NordeaProject.Data;

namespace NordeaProject.DataSource
{
    public interface IPositionDataSource
    {
        Position[] GetPositions();
    }
}
