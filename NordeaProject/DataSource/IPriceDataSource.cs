﻿using NordeaProject.Data;

namespace NordeaProject.DataSource
{
    public interface IPriceDataSource
    {
        Price[] GetPrices();
    }
}