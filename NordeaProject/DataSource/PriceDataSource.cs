﻿using System;
using System.Collections.Generic;
using NordeaProject.Data;

namespace NordeaProject.DataSource
{
    public class PriceDataSource: IPriceDataSource
    {
        public Price[] GetPrices()
        {
            var date = new DateTime(2000, 01, 01);
            Random randNum = new Random();
            var result = new List<Price>();
            for (int i = 0; i < 1000; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    result.Add(new Price(date.AddDays(j), i.ToString(), randNum.Next(1, 10000)));
                }
            }
            return result.ToArray();
        }
    }
}