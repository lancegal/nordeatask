﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NordeaProject.Data;

namespace NordeaProject.WebServicesConnectors
{
    public class WebServiceThreeConnector: IWebServiceConnector
    {
        private int _millisecondsTimeout = 10000;
        public async Task<Price[]> GetPricesFromWebService()
        {
            await Task.Factory.StartNew(() => Thread.Sleep(_millisecondsTimeout));
            return new[] { new Price(new DateTime(1991, 1, 1), "b", 1) };
        }
    }
}
