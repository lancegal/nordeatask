﻿using System.Threading.Tasks;
using NordeaProject.Data;

namespace NordeaProject.WebServicesConnectors
{
    public interface IWebServiceConnector
    {
        Task<Price[]> GetPricesFromWebService();
    }
}
