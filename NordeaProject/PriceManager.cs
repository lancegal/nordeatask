﻿using System.Collections.Generic;
using System.Configuration;
using NordeaProject.Data;
using NordeaProject.DataSource;
using NordeaProject.InterpolationAlgotithms;

namespace NordeaProject
{
    public class PriceManager
    {
        private readonly IPriceDataSource _priceDataSource;

        public PriceManager(IPriceDataSource priceDataSource)
        {
            _priceDataSource = priceDataSource;
        }

        //It could be send by constructor, but for this project this place is the best for it.
        private readonly Dictionary<string, IInterpolationAlgorithm> _listOfAlgorithms = new Dictionary<string, IInterpolationAlgorithm>()
        {
            { nameof(InterpolationAlgorithmOne), new InterpolationAlgorithmOne() },
            { nameof(InterpolationAlgorithmTwo), new InterpolationAlgorithmTwo() }
        };

        public Price[] GetPrices()
        {
            string choosenAlgorithmName = ConfigurationManager.AppSettings["InterpolationAlgorithm"];
            if (choosenAlgorithmName == null)
            {
                throw new ConfigurationErrorsException("You are missing InterpolationAlgorithm in config file");
            }
            if (!_listOfAlgorithms.ContainsKey(choosenAlgorithmName))
            {
                throw new ConfigurationErrorsException($"Algorithm {choosenAlgorithmName} is not avaible");
            }
            var priceData = _priceDataSource.GetPrices();
            return _listOfAlgorithms[choosenAlgorithmName].GetInterpolatedPrices(priceData);
        }

    }

}
