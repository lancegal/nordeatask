﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NordeaProject.Data;

namespace NordeaProject.InterpolationAlgotithms
{
    interface IInterpolationAlgorithm
    {
        Price[] GetInterpolatedPrices(Price[] pricesFromSource);
    }
}
