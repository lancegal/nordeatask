﻿using System;
using System.Collections.Generic;
using System.Linq;
using NordeaProject.Data;
using NordeaProject.DataSource;

namespace NordeaProject
{
    public class TaskOne
    {
        private readonly IPriceDataSource _priceDataSource;
        private readonly IPositionDataSource _positionDataSource;

        public TaskOne(IPriceDataSource priceDataSource, IPositionDataSource positionDataSource)
        {
            _priceDataSource = priceDataSource;
            _positionDataSource = positionDataSource;
        }
        public Dictionary<Position, decimal> CalculateMarketValue()
        {
            var result = new Dictionary<Position, decimal>();
            var prices = _priceDataSource.GetPrices(); // can be more than 100000 records
            var positions = _positionDataSource.GetPositions(); // can be more than 1000 records

            var generatePircesByKey = prices.ToDictionary(e => (e.ProductKey,e.Date));

            foreach (var position in positions)
            {
                if(!generatePircesByKey.ContainsKey((position.ProductKey, position.Date)))
                    throw new Exception("Position without connected price!");
                var priceForPosition = generatePircesByKey[(position.ProductKey, position.Date)];
                result.Add(position, (decimal)priceForPosition.Value * position.Amount);
            }

            return result;
        }
    }
}
