﻿using System;

namespace NordeaProject.Data
{
    /// <summary>
    /// Market price of certain product at certain date
    /// </summary>
    public class Price
    {
        public Price(DateTime date, string productKey, double value)
        {
            Date = date;
            ProductKey = productKey;
            Value = value;
        }

        public DateTime Date { get; }
        public string ProductKey { get; }
        public double Value { get; }
    }

}
