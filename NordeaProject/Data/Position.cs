﻿using System;

namespace NordeaProject.Data
{
    /// <summary>
    /// Represents amount of certain product that was present in client's portfolio at certain date
    /// </summary>
    public class Position
    {
        public Position(int positionId, DateTime date, decimal amount, string productKey)
        {
            PositionId = positionId;
            Date = date;
            Amount = amount;
            ProductKey = productKey;
        }

        public int PositionId { get; }
        public DateTime Date { get; }
        public decimal Amount { get; }
        public string ProductKey { get; }
    }

}
