﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NordeaProject.Data;
using NordeaProject.WebServicesConnectors;

namespace NordeaProject
{
    public class TaskTwo
    {
        private readonly IWebServiceConnector[] _connectors;

        public TaskTwo(params IWebServiceConnector[] connectors)
        {
            _connectors = connectors;
        }
        public async Task<IEnumerable<Price>> GetPrices()
        {
            var connectorTasks = _connectors.Select(e => e.GetPricesFromWebService()).ToList();

            await Task.WhenAll(connectorTasks);

            var result = new List<Price>();
            foreach (var task in connectorTasks)
            {
                result.AddRange(task.Result);
            }

            return result.DistinctBy(e => (e.Date, e.ProductKey));
        }

    }
}
