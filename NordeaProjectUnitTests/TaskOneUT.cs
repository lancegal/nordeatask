﻿using System;
using NordeaProject;
using NordeaProject.Data;
using NordeaProject.DataSource;
using NUnit.Framework;
using Rhino.Mocks;

namespace NordeaProjectUnitTests
{
    [TestFixture]
    public class TaskOneUt
    {
        [Test]
        public void CalculateMarketValue_AllProperScenario_ReturnPositions()
        {
            var expectedPosition = new Position(1, new DateTime(1991, 1, 1), 1, "a");

            var mock1 = MockRepository.GenerateStub<IPriceDataSource>();
            mock1.Stub(e => e.GetPrices()).Return(new[]
                {new Price(new DateTime(1991, 1, 1),"a", 1) });
            var mock2 = MockRepository.GenerateStub<IPositionDataSource>();
            mock2.Stub(e => e.GetPositions()).Return(new[]{expectedPosition});
            var sut = new TaskOne(mock1, mock2);


            var actual = sut.CalculateMarketValue();


            Assert.That(actual.ContainsKey(expectedPosition), Is.True);
            Assert.That(actual[expectedPosition], Is.EqualTo(1));
        }
        [Test]
        public void CalculateMarketValue_ThereIsPositionThatDoNotHavePrice_ReturnException()
        {
            var mock1 = MockRepository.GenerateStub<IPriceDataSource>();
            mock1.Stub(e => e.GetPrices()).Return(new[]
                {new Price(new DateTime(1991, 1, 1),"a",1) });
            var mock2 = MockRepository.GenerateStub<IPositionDataSource>();
            mock2.Stub(e => e.GetPositions()).Return(new[]
                {new Position(1, new DateTime(1991, 1, 1),1,"b") });
            var sut = new TaskOne(mock1, mock2);


            Assert.Throws<Exception>(() => sut.CalculateMarketValue());
        }
    }
}
