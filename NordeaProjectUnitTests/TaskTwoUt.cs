﻿using System;
using System.Threading.Tasks;
using NordeaProject;
using NordeaProject.Data;
using NordeaProject.WebServicesConnectors;
using NUnit.Framework;
using Rhino.Mocks;

namespace NordeaProjectUnitTests
{
    [TestFixture]
    public class TaskTwoUt
    {
        [Test]
        public async void GetPrices_TwoPricesWith()
        {
            var expected = new []
            {
                new Price(new DateTime(1991, 1, 1), "a", 1),
                new Price(new DateTime(1991, 1, 1), "b", 1)
            };
            var a = CreateConnectorStub("a");
            var b = CreateConnectorStub("a");
            var c = CreateConnectorStub("b");

            //Here should be done some mocks: but these connector are stubs already
            var sut = new TaskTwo(a, b, c);

            var actual = await sut.GetPrices();

            Assert.That(actual, Is.EquivalentTo(expected));
        }

        private static IWebServiceConnector CreateConnectorStub(string productKey)
        {
            var connectorStub = MockRepository.GenerateStub<IWebServiceConnector>();
            connectorStub.Stub(e => e.GetPricesFromWebService()).Return(new Task<Price[]>(() => new[] {new Price(new DateTime(1991, 1, 1), productKey, 1) }));
            return connectorStub;
        }
    }
}
